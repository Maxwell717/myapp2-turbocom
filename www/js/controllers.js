angular.module('starter.controllers', [])

.factory ('dataShare', function(){
  var catalogues = [
    {id: 00, title: '--------Выберите ТК--------', corner_a: '0', corner_b: '0'},
    {id: 01, title: 'ТКР-6 (01)',               img: 'tkr-6-01', engine: 'Д-245.27/.С-435/.С-437/.5/.5С-439', transp: 'МТЗ-922, 923 ЮМЗ, ВТЗ, ЛТЗ, комбайн Гомсельмаш ДСТ Амкодор МТЗ', weight: '6,5', corner_a: '0',corner_b: '0', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-00.01, ТКР 6-01.01', analogCzSt: 'С14-126-01, С14-127-02', analogServTurb: 'ТКР-6СТ-00', analogTurbTech: '', analogSwitzer: ''},
    {id: 02, title: 'ТКР-6 (02)',               img: 'tkr-6-02', engine: 'Д-245.12С-143/-231/-365/-368', transp: 'ЗиЛ 4331 (Бычек)', weight: '6,5', corner_a: '99',corner_b: '90', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-00.02', analogCzSt: 'С14-127-01', analogServTurb: 'ТКР-7СТ-04'},
    {id: 03, title: 'ТКР-6 (03)',               img: 'tkr-6-03', engine: 'РМ-80; РМ-120', transp: 'МТЗ 100, ЗиЛ 5301', weight: '6,5', corner_a: '280',corner_b: '10', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-00.03', analogCzSt: '', analogServTurb: 'ТКР-7СТ-01, 02'},
    {id: 04, title: 'ТКР-6 (04)',               img: 'tkr-6-04', engine: 'Д-245.12С', transp: '33 ГТ, ГАЗ 34039', weight: '6,5', corner_a: '192',corner_b: '0', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-00.04', analogCzSt: '', analogServTurb: ''},
    {id: 05, title: 'ТКР-6 (05)',               img: 'tkr-6-05', engine: 'Д-245.7-628; Д-245.7-658', transp: 'ГАЗ 3309, 33081', weight: '6,5', corner_a: '250',corner_b: '110', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-02.05', analogCzSt: '', analogServTurb: ''},
    {id: 06, title: 'ТКР-6 (06)',               img: 'tkr-6-06', engine: 'Д-246.3; Д-246.4', transp: 'Энергоустановка', weight: '6,5', corner_a: '340',corner_b: '110', DWheelComp: '61/40,5', DWheelTurb: '61/49,6', frequency: '115000', maxp: '2,1', prod: '0,15±0,001', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,71', analogBZA: 'ТКР 6-00.06', analogCzSt: '', analogServTurb: ''},
    {id: 07, title: 'ТКР-6.1 (01)',             img: 'tkr-61-01-s-klapanom', engine: 'Д-245.7; Д-245.9', transp: 'Автобусы ПАЗ 32051 Автомобили ЗиЛ', weight: '8,4', corner_a: '340',corner_b: '110', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,76', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-07.01, ТКР 6.1-08.01', analogCzSt: 'С14-194-01 С14-197-01', analogServTurb: 'ТКР-6СТ-01', analogTurbTech: 'ТКР-60-14'},
    {id: 08, title: 'ТКР-6.1 (02)',             img: 'tkr-61-02-s-klapanom', engine: 'Д-245.9-67; Д-245.9-568', transp: 'ЛАЗ 695; Автобусы ПАЗ - "Аврора"', weight: '8,4', corner_a: '348',corner_b: '0', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-05.02', analogCzSt: 'С14-174-01', analogServTurb: ''},
    {id: 09, title: 'ТКР-6.1 (03)',             img: 'tkr-61-03-s-klapanom', engine: 'Д-245.7-566; Д-245.7-165', transp: 'Автомобили ГАЗ', weight: '8,4', corner_a: '250',corner_b: '110', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-05.03, ТКР 6.1-06.03, ТКР 6.1-09.03', analogCzSt: 'С14-179-02 С14-179-01', analogServTurb: ''},
    {id: 10, title: 'ТКР-6.1 (04)',             img: 'tkr-61-04-s-klapanom', engine: 'Д-245.16Л-261; Д-245.9-67 (568)', transp: 'Онежский тракторный з-д ТДТ-55А; ЛХТ-55; ТЛТ-100А', weight: '8,4', corner_a: '310',corner_b: '110', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-03.04, ТКР 6.1-04.04', analogCzSt: 'С14-101-02', analogServTurb: ''},
    {id: 11, title: 'ТКР-6.1 (05)',             img: 'tkr-61-05-s-klapanom', engine: 'Д-245.9-335; Д-245.9-336', transp: 'МАЗ 4370', weight: '8,4', corner_a: '25',corner_b: '110', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-03.05', analogCzSt: '', analogServTurb: ''},
    {id: 12, title: 'ТКР-6.1 (06)',             img: 'tkr-61-06-s-klapanom', engine: 'Д-245.7 Е2', transp: 'ГАЗ, ВАЛДАЙ', weight: '8,4', corner_a: '330',corner_b: '230', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-10.06', analogCzSt: 'С14-180-01', analogServTurb: ''},
    {id: 13, title: 'ТКР-6.1 (07)',             img: 'tkr-61-07-s-klapanom', engine: 'Д-245.9 Е2', transp: 'ЗИЛ, ПАЗ', weight: '8,4', corner_a: '205',corner_b: '110', DWheelComp: '61/42', DWheelTurb: '62,6/49,6', frequency: '120000', maxp: '1,52', prod: '0,35', maxC: '700', adiabKPD: '0,72', effectiveKPD: '0,65', analogBZA: 'ТКР 6.1-11.07, ТКР 6.1-12.07', analogCzSt: 'С14-196-01', analogServTurb: ''},
    {id: 14, title: 'ТКР-6,5.1 (09.03) ЕВРО-3', img: 'tkr-651-0903', engine: 'Д-245.7E3', transp: 'ГАЗ-3308/ 09', weight: '9,1', corner_a: '250',corner_b: '110', DWheelComp: '67,9', DWheelTurb: '60,5', frequency: '135000', maxp: '1,54', prod: '0,25', maxC: '700', adiabKPD: '0,73', effectiveKPD: '0,71', analogBZA: ' 	ТКР 6,5.1-09.03', analogCzSt: 'C14-179-02', analogServTurb: ''},
    {id: 15, title: 'ТКР-6,5.1 (10.06) ЕВРО-3', img: 'tkr-651-1006', engine: 'Д-245.7E3', transp: 'Валдай, ГАЗ-33104', weight: '9,1', corner_a: '330',corner_b: '230', DWheelComp: '67,9', DWheelTurb: '60,5', frequency: '135000', maxp: '1,54', prod: '0,25', maxC: '700', adiabKPD: '0,73', effectiveKPD: '0,71', analogBZA: 'ТКР 6,5.1-10.06', analogCzSt: 'C14-180-01', analogServTurb: ''},
    {id: 16, title: 'ТКР-700 (01)',             img: 'tkr-700-01', engine: 'Д-260.1(С)/.2(С)/.1С2/.2С2/.1/.2', transp: 'К-3000, МТЗ-1523, -1221, Амкодор', weight: '9,7', corner_a: '90',corner_b: '90', DWheelComp: '81,3/54,5', DWheelTurb: '73,5/64,5', frequency: '100000', maxp: '1,45 - 2,5', prod: '0,25', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,7', analogBZA: 'ТКР 7-00.01', analogCzSt: 'К-27-61-02', analogServTurb: '', analogTurbTech: 'ТКР - 7Н3'},
    {id: 17, title: 'ТКР-700 (02)',             img: 'tkr-700-02', engine: 'Д-260.4-16; Д-260.4-18', transp: 'К3000, МТЗ1523, Гомсельмаш', weight: '9,7', corner_a: '0',corner_b: '0', DWheelComp: '81,3/54,5', DWheelTurb: '73,5/64,5', frequency: '100000', maxp: '1,45 - 2,5', prod: '0,25', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,7', analogBZA: 'ТКР 7-00.02', analogCzSt: 'К-27-61-02', analogServTurb: ''},
    {id: 18, title: 'ТКР-7',                    img: 'tkr-7', engine: 'Д-440; Д-442 (АМЗ)', transp: 'Комбайн "Енисей-1200"', weight: '7,8', corner_a: '90',corner_b: '0', DWheelComp: '76/49', DWheelTurb: '75,5/62,5', frequency: '60000', maxp: '1,45-2,5', prod: '0,15-0,25', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,68', analogBZA: '', analogCzSt: '', analogServTurb: ''},
    {id: 19, title: 'ТКР-7Н-1 (01) правая',     img: 'tkr-7n-1-01-pr', engine: 'КамАЗ-7403.10; 740.11-240 и др.', transp: 'КамАЗ', weight: '7,8', corner_a: '310',corner_b: '310', DWheelComp: '76/48', DWheelTurb: '76/48', frequency: '60000', maxp: '1,8-2,2', prod: '0,12', maxC: '600', adiabKPD: '0,66', effectiveKPD: '0,68', analogBZA: 'ТКР 7Н-1Б', analogCzSt: 'К27-145-01, К27-91-01', analogServTurb: 'ТКР-7СТ-05', analogTurbTech: 'ТКР-7СТ-05'},
    {id: 20, title: 'ТКР-7Н-1 (02) левая',      img: 'tkr-7n-1-02-lev', engine: 'КамАЗ-7403.10; 740.11-240 и др.', transp: 'КамАЗ', weight: '7,8', corner_a: '60',corner_b: '90', DWheelComp: '76/48', DWheelTurb: '76/48', frequency: '60000', maxp: '1,8-2,2', prod: '0,12', maxC: '600', adiabKPD: '0,66', effectiveKPD: '0,68', analogBZA: 'ТКР 7Н-1Б', analogCzSt: 'К27-145-01, К27-91-01', analogServTurb: 'ТКР-7H1-СТ', analogTurbTech: 'ТКР-7H1К'},
    {id: 21, title: 'ТКР-7C-6 (01) правая',     img: 'tkr-7s-6-01-4-shpil-pr', engine: 'КамАЗ-740.30-260/.31-260/.50-360/.51-320', transp: 'КамАЗ Евро-2', weight: '9,7', corner_a: '310',corner_b: '330', DWheelComp: '76,2/49,4', DWheelTurb: '73,5/64,3', frequency: '100000', maxp: '1,65-1,9', prod: '0,27', maxC: '700', adiabKPD: '0,7', effectiveKPD: '0,75', analogBZA: '', analogCzSt: 'К27-145-01', analogServTurb: 'ТКР-К27-145СТ, ТКР-7С6.06.02', analogTurbTech: 'ТКР - 700', analogSwitzer: 'S2В/7624 ТАЕ/0,76D9'},
    {id: 22, title: 'ТКР-7C-6 (02) левая',      img: 'tkr-7s-6-02-4-shpil-lev', engine: 'КамАЗ-740.30-260/.31-260/.50-360/.51-320', transp: 'КамАЗ Евро-2', weight: '9,7', corner_a: '60',corner_b: '90', DWheelComp: '76,2/49,4', DWheelTurb: '73,5/64,3', frequency: '100000', maxp: '1,65-1,9', prod: '0,27', maxC: '700', adiabKPD: '0,7', effectiveKPD: '0,75', analogBZA: '', analogCzSt: 'К27-145-02', analogServTurb: 'ТКР-7СТ-06.02, ТКР-К27-145СТ', analogSwitzer: 'S2В/7624 ТАЕ/0,76D9'},
    {id: 23, title: 'ТКР-К-27-115 (01) правая', img: 'tkr-k-27-115-01-pr', engine: 'КамАЗ 740.11-240; 13-260; 14-300; 30-260; 31-240; 50-360; 51-320', transp: 'КамАЗ 740.13; 740.14', weight: '-', corner_a: '338',corner_b: '321', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-08.09', analogCzSt: 'К27-115/145', analogServTurb: '', analogSwitzer: ''},
    {id: 24, title: 'ТКР-К-27-115 (02) левая',  img: 'tkr-k-27-115-02-lev', engine: 'КамАЗ 740.11-240; 13-260; 14-300; 30-260; 31-240; 50-360; 51-320', transp: 'КамАЗ 740.13; 740.14', weight: '-', corner_a: '36',corner_b: '90', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-08.10 (лев.)', analogCzSt: 'К27-115/145', analogServTurb: '', analogSwitzer: ''},
    {id: 25, title: 'ТКР-К-27-145 (01) правая', img: 'tkr-k-27-145-01-pr', engine: 'КамАЗ 740.11-240; 13-260; 14-300; 30-260; 31-240; 50-360; 51-320', transp: 'КамАЗ 740.13; 740.14', weight: '-', corner_a: '338',corner_b: '321', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-08.09', analogCzSt: 'К27-115/145', analogServTurb: '', analogSwitzer: ''},
    {id: 26, title: 'ТКР-К-27-145 (02) левая',  img: 'tkr-k-27-145-02-lev', engine: 'КамАЗ 740.11-240; 13-260; 14-300; 30-260; 31-240; 50-360; 51-320', transp: 'КамАЗ 740.13; 740.14', weight: '-', corner_a: '36',corner_b: '90', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-08.10 (лев.)', analogCzSt: 'К27-115/145', analogServTurb: '', analogSwitzer: ''},
    {id: 27, title: 'ТКР-К-27-61 (01)',         img: 'tkr-k-27-61-01', engine: 'Д-260.4 (Брянск, Арсенал), Ирмаш, Ростельмаш', transp: 'Автобус МАЗ-103', weight: '-', corner_a: '310',corner_b: '0', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-00.02', analogCzSt: 'К27-61-01', analogServTurb: '', analogSwitzer: ''},
    {id: 28, title: 'ТКР-К-27-61 (02)',         img: 'tkr-k-27-61-02', engine: 'Д-260.1; Д-260.2 (МТЗ-1222/1522; Ростсельмаш, Амкодор)', transp: 'Трактор МТЗ-1221, МТЗ-1222, МТЗ-1222D,МТЗ-1523', weight: '-', corner_a: '108',corner_b: '90', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '0,72', analogBZA: 'ТКР 7-00.01', analogCzSt: 'К27-61-02', analogServTurb: '', analogSwitzer: ''},
    {id: 29, title: 'ТКР-К-27 TML',             img: 'tkr-k-27-tml', engine: 'ТАТА 697 TC55L EURO II', transp: 'Автобус Эталон БАЗ-А079, TATA, I-VAN', weight: '-', corner_a: '310',corner_b: '180', DWheelComp: '-', DWheelTurb: '-', frequency: '30000-120000', maxp: '2,9', prod: '2,6', maxC: '700', adiabKPD: '0,75', effectiveKPD: '', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''},
    {id: 30, title: 'ТКР-7Н-2А',                img: 'tkr-7n-2a', engine: 'Д-245; Д-245.1; РМ-80', transp: 'ЗиЛ-5301/4331/130, ПАЗ, ЧАЗ, ЛАЗ-695, МТЗ-100/922/923', weight: '7,8', corner_a: '90',corner_b: '90', DWheelComp: '76/49', DWheelTurb: '76/63', frequency: '60000', maxp: '1,9', prod: '0,1-0,3', maxC: '700', adiabKPD: '0,66', effectiveKPD: '0,68', analogBZA: 'ТКР-7Н-2А', analogCzSt: 'К-27-61-02', analogServTurb: 'ТКР-7СТ-03', analogTurbTech: 'ТКР - 7Н2А'},
    {id: 31, title: 'ТКР-8,5С',                 img: 'tkr-85s', engine: 'Д-160М', transp: 'Трактор Т-170М.01', weight: '13,2', corner_a: '-',corner_b: '-', DWheelComp: '85/60', DWheelTurb: '85/70', frequency: '80000', maxp: '2', prod: '0,2', maxC: '650', adiabKPD: '0,75-0,77', effectiveKPD: '0,7', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''},
    {id: 32, title: 'ТКР-9-12 (00)',            img: 'tkr-9-12-00', engine: 'ЯМЗ-238Б/БВ/БЛ/БК/БН/ДК/Д/БЕ-1/ДЕ/НД6/НД7/НД8; ЯМЗ-7512.10/-7513.10/-7514.1', transp: 'МАЗ-54323,-5516, КрАЗ-6503, 6505, КрАЗ-6343, МАЗ-64229, «Урал», самосвалы МоАЗ', weight: '18', corner_a: '75',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: 'ТКР 10', analogCzSt: 'К-36-87-01, К-36-30-01', analogServTurb: 'ТКР-7СТ-09', analogTurbTech: 'ТКР-100'},
    {id: 33, title: 'ТКР-9-12 (01)',            img: 'tkr-9-12-01-pr', engine: 'ЯМЗ-240НМ2; ЯМЗ-240ПМ2', transp: 'БелАЗ-75485,75486,75487', weight: '18', corner_a: '313,5',corner_b: '315', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: 'К-36-88-01', analogServTurb: '', analogTurbTech: ''},
    {id: 34, title: 'ТКР-9-12 (02)',            img: 'tkr-9-12-02-lev', engine: 'ЯМЗ-240НМ2; ЯМЗ-240ПМ2', transp: 'БелАЗ-75485,75486,75487', weight: '18', corner_a: '65',corner_b: '90', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: 'К-36-88-02', analogServTurb: '', analogTurbTech: ''},
    {id: 35, title: 'ТКР-9-12 (03)',            img: 'tkr-9-12-03-pr', engine: 'ЯМЗ-8401.10-3/-5/-6/-14/-24; ЯМЗ-845.10; ЯМЗ-8502.10', transp: 'БелАЗ-75482, МАЗ-7413', weight: '18', corner_a: '52,5',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100-03'},
    {id: 36, title: 'ТКР-9-12 (04)',            img: 'tkr-9-12-04-pr', engine: 'ЯМЗ-8401.10-3/-5/-6/-14/-24; ЯМЗ-845.10; ЯМЗ-8502.10', transp: 'БелАЗ-75482, МАЗ-7413', weight: '18', corner_a: '340',corner_b: '60', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100-04'},
    {id: 37, title: 'ТКР-9-12 (07)',            img: 'tkr-9-12-07', engine: 'ЯМЗ-236Н/Б/БЕ/НЕ/БЕ2/НЕ2/НД; ЯМЗ-7601.10', transp: 'Автомобили МАЗ, ЗИЛ, «Урал», КрАЗ; автобусы ЛАЗ двигатели ЯМЗ с рядным ТНВД', weight: '18', corner_a: '90',corner_b: '23', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-90'},
    {id: 38, title: 'ТКР-9-12 (08)',            img: 'tkr-9-12-08-pr', engine: '???', transp: 'Тракторы ЧЗПТ', weight: '18', corner_a: '37,5',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''},
    {id: 39, title: 'ТКР-9-12 (09)',            img: 'tkr-9-12-09-lev', engine: 'ЯМЗ-850.10', transp: 'Тракторы ЧЗПТ', weight: '18', corner_a: '340',corner_b: '60', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100-09'},
    {id: 40, title: 'ТКР-9-12 (10)',            img: 'tkr-9-12-10-lev', engine: 'ЯМЗ-8501.10', transp: 'Промышленный трактор-бульдозер Т-25.01Я; трактор мелиоративный ТМ-25.01 (ОАО "Промтрактор")', weight: '18', corner_a: '0',corner_b: '60', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100-10'},
    {id: 41, title: 'ТКР-9-12 (11)',            img: 'tkr-9-12-11', engine: 'ЯМЗ-238НД3/НД4/НД5', transp: 'Тракторы К-701А "Кировец"', weight: '18', corner_a: '75',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100'},
    {id: 42, title: 'ТКР-9-12 (13)',            img: 'tkr-9-12-13', engine: '???', transp: 'автомобили ЗиЛ', weight: '18', corner_a: '60',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-90-13'},
    {id: 43, title: 'ТКР-9-12 (14)',            img: 'tkr-9-12-14', engine: 'ЯМЗ-236НЕ2-3/-23/-24; ЯМЗ-7601.10-18', transp: 'автомобили УрАЛ', weight: '18', corner_a: '135',corner_b: '330', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-90-14'},
    {id: 44, title: 'ТКР-9-12 (19)',            img: 'tkr-9-12-19-pr', engine: 'ЯМЗ-8501.10', transp: 'Промышленный трактор-бульдозер Т-25.01Я; Трактор мелиоративный ТМ-25.01 (ОАО "Промтрактор")', weight: '18', corner_a: '37,5',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/86,6', frequency: '50000', maxp: '1,8', prod: '0,25-0,35', maxC: '650', adiabKPD: '0,8', effectiveKPD: '0,64', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: 'ТКР-100-19'},
    {id: 45, title: 'ТКР-К-36-87-01',           img: 'tkr-k-36-t-87-01', engine: '238Б, 238БЕ, 238БЕ2, 238Д, 238ДЕ, 238ДЕ2, ЯМЗ-7511.10, ЯМЗ-7512.10, 7513.10, 7514.10', transp: 'МАЗ-54323, 53362, УРАЛ-5323, КрАЗ-5444, 6503, 6505, БАЗ-69506, 69531, МоАЗ-75051, 69084, 4901, 40489, КрАЗ-7133, МАЗ-64229, 6303, 5516, 5552, 53363, КрАЗ-6443, 64432, 6437, 643711, МЗКТ-65158, 692388, МЛ-107 г. Курган, МАЗ-533605, 630305', weight: '18,3', corner_a: '75',corner_b: '0', DWheelComp: '102/68,3', DWheelTurb: '96,5/81,12', frequency: '45000', maxp: '0,2-0,55', prod: '0,27', maxC: '700', adiabKPD: '0,8', effectiveKPD: '0,65', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''},
    {id: 46, title: 'ТКР-К-36-88-01',           img: 'tkr-k-36-t-88-01-pr', engine: 'ЯМЗ-240НМ2/ПМ2', transp: 'БелАЗ-75485, 75486, 75487, 75489, БелАЗ-7540,75401, 75406, 75408, 75409', weight: '18,3', corner_a: '314',corner_b: '315', DWheelComp: '102/68,3', DWheelTurb: '96,5/81,12', frequency: '45000', maxp: '0,2-0,55', prod: '0,27', maxC: '700', adiabKPD: '0,8', effectiveKPD: '0,65', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''},
    {id: 47, title: 'ТКР-К-36-88-02',           img: 'tkr-k-36-t-88-02', engine: 'ЯМЗ-240НМ2/ПМ2', transp: 'БелАЗ-75485, 75486, 75487, 75489, БелАЗ-7540,75401, 75406, 75408, 75409', weight: '18,3', corner_a: '65',corner_b: '90', DWheelComp: '102/68,3', DWheelTurb: '96,5/81,12', frequency: '45000', maxp: '0,2-0,55', prod: '0,27', maxC: '700', adiabKPD: '0,8', effectiveKPD: '0,65', analogBZA: '', analogCzSt: '', analogServTurb: '', analogTurbTech: ''}
  ];
  return {
    all: function() {
      return catalogues;
    }
  };
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('CatalogueCtrl', function($scope, dataShare) {
    $scope.catalogues = dataShare.all();

})

.controller('CatalogueUnitCtrl', function($scope, $stateParams, dataShare) {
    $scope.data = dataShare.all();
    $scope.catalogueUnit = $scope.data[$stateParams.catalogueId];
})

.controller('DispAngleAppCtrl', function ($scope, dataShare) {
    angular.element(document).ready(function() {
      $scope.angle1 = 0;
      $scope.angle2 = 0;
    });


    $scope.minmax_a = function (value, min, max) {
      var input1 = document.getElementsByTagName('input')[0];
      input1.value = parseInt(value);
      if (! value || parseInt(value) == 0 || parseInt(value) < min)
        $scope.angle1 = 0;
      else if (parseInt(value) > max || isNan(value))
        $scope.angle1 = 360;
      else $scope.angle1 = parseInt(value);
    };
    $scope.minmax_b = function (value, min, max) {
      var input2 = document.getElementsByTagName('input')[1];
      input2.value = parseInt(value);
      if (value == null || parseInt(value) == 0 || parseInt(value) < min)
        return $scope.angle2 = 0;
      else if (parseInt(value) > max || isNan(value))
        return $scope.angle2 = 360;
      else return $scope.angle2 = parseInt(value);
    };

    $scope.change_val_keyup_a = function (value) {
      $scope.angle1 = value;
    };
    $scope.change_val_keyup_b = function (value) {
      $scope.angle2 = value;
    };


    $scope.data = dataShare.all();
    $scope.angle3 = $scope.data[0];

    $scope.change_val_select = function (value) {
      $scope.angle1 = value.corner_a;
      $scope.angle2 = value.corner_b;
    };
});
